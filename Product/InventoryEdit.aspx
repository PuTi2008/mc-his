﻿<%@ page language="C#" masterpagefile="~/Commons/Simple.master" autoeventwireup="true" inherits="Product_InventoryEdit, App_Web_r2wuchfj" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphSimple" Runat="Server">

    <script>

        function setQty(qty) {

            window.parent.document.getElementById('ctl00_cphMain_txtQty').value = qty;

            window.parent.$.fancybox.close();
        }

    </script>

      <div class="content-wrapper-simple">

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:Label ID="lblTitle" runat="server"></asp:Label>
              </h3>
            </div>

            <div class="box-body">
                
              <div class="row">
              
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label2" runat="server" Text="数量"></asp:Label></label>
                    <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Text="10"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="备注"></asp:Label></label>
                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                  </div>
                  
              </div>
                
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnGet" runat="server" Text="入库" CssClass="btn btn-primary" onclick="btnGet_Click" OnClientClick="{return confirm('确定入库吗？');}"></asp:Button>
                <asp:Button ID="btnPost" runat="server" Text="出库" CssClass="btn btn-success" onclick="btnPost_Click" OnClientClick="{return confirm('确定出库吗？');}"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>