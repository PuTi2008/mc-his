﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Task_View, App_Web_xju2gzr0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            任务查看
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">任务查看</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <asp:HyperLink ID="hlPrint" runat="server" NavigateUrl="javascript:McPrintHide('.box-title,.box-footer');"><span class="label label-primary"><i class="fa fa-print"></i> 打印</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">
              <div class="row">
              
                <div class="box-body">
                  <div class="mailbox-read-info">
                    <h3><asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
                    <h5><asp:Label ID="lblFrom" runat="server" CssClass="mailbox-read-time"></asp:Label><asp:Label ID="lblDate" runat="server" CssClass="mailbox-read-time pull-right"></asp:Label></h5>
                  </div>
                  <div class="mailbox-read-message">
                      <asp:Label ID="lblDescription" runat="server"></asp:Label>
                      <asp:Label ID="lblFeedback" runat="server"></asp:Label>
                      <asp:Label ID="lblNote" runat="server"></asp:Label>
                  </div>
                </div>

              </div>

              <div class="row">
              
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label5" runat="server" Text="状态"></asp:Label></label>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label2" runat="server" Text="反馈"></asp:Label></label>
                    <asp:TextBox ID="txtFeedback" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
              
              <div class="row">
                  
                <div class="col-md-6 form-group">
                <div class="btn btn-default btn-file">
                    <i class="fa fa-paperclip"></i> 增加附件
                    <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);"></asp:FileUpload>
                    <span id="filepath"></span>
                </div>
                </div>

              </div>

            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnBack" runat="server" Text="返回" CssClass="btn btn-default" onclick="btnBack_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>


