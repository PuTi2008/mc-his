﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Address_Edit, App_Web_rnejanru" %>

<%@ Register Src="Nav.ascx" TagName="Nav" TagPrefix="MojoCube" %>

<%@ Register Src="~/Controls/CKeditor.ascx" TagName="CKeditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function ddlChange() {
            var ddl = document.getElementById("ctl00_cphMain_ddlType");
            if (ddl.value == "2") {
                $("#ctl00_cphMain_txtReceiver").removeAttr("onfocus");
                document.getElementById("ctl00_cphMain_txtReceiver").value = "";
                document.getElementById("ctl00_cphMain_txtReceiverID").value = "";
                document.getElementById("ctl00_cphMain_ShareDiv").style.display = "";
            }
            else {
                document.getElementById("ctl00_cphMain_txtReceiver").value = "";
                document.getElementById("ctl00_cphMain_txtReceiverID").value = "";
                document.getElementById("ctl00_cphMain_ShareDiv").style.display = "none";
            }
        }

        function removeAtt() {
            document.getElementById("ctl00_cphMain_txtImagePath").value = "";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            联系人编辑
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">联系人编辑</li>
          </ol>
        </section>

        <section class="content">
            <div class="row">
        
                <MojoCube:Nav id="Nav" runat="server" />
    
                <div class="col-md-9">
                
                  <div id="AlertDiv" runat="server"></div>

                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">
                          <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                      </h3>
                    </div>
                    <div class="box-body">
                      <div class="form-group">
                         <asp:DropDownList ID="ddlFolder" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                      <div class="form-group">
                         <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2" onchange="ddlChange();"></asp:DropDownList>
                      </div>
                      <div id="ShareDiv" runat="server" class="form-group" style="position:relative; display:none">
                        <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" placeholder="分享给：" onfocus="this.blur()"></asp:TextBox>
                        <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                        <div style="position:absolute; top:5px; right:5px;">
                            <asp:HyperLink ID="hlShare" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-share-alt"></i> 分享</span></asp:HyperLink>
                        </div>
                      </div>
                      <div class="form-group">
                         <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                      <div class="form-group">
                        <asp:DropDownList ID="ddlSex" runat="server" CssClass="form-control select2">
                            <asp:ListItem Text="男" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="女" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control" placeholder="联系人："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtPhone1" runat="server" CssClass="form-control" placeholder="手机号码："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtPhone2" runat="server" CssClass="form-control" placeholder="公司电话："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" placeholder="传真："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="Email："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" placeholder="地址："></asp:TextBox>
                      </div>
                      <div class="form-group">
                        <asp:TextBox ID="txtNote" runat="server" CssClass="form-control" placeholder="备注："></asp:TextBox>
                      </div>

                      <div class="row">
              
                          <hr />
              
                          <div class="col-md-6 form-group">
                            <label><asp:Label ID="Label17" runat="server" Text="头像"></asp:Label></label>
                            <div class="user-edit-image"><asp:Image ID="imgPortrait" runat="server"></asp:Image></div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <i class="fa fa-upload"></i> 上传头像
                                    <asp:FileUpload ID="fuPortrait" runat="server" onchange="ChkUploadImage(this,ctl00_cphMain_imgPortrait);"></asp:FileUpload>
                                </div>
                                <p class="help-block">尺寸在512*512以内，大小在500KB以内</p>
                            </div>
                          </div>
                  
                      </div>

                      <div class="row">
                      
                          <div class="col-md-6 form-group" style="position:relative;">
                            <asp:TextBox ID="txtImagePath" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                            <div style="position:absolute; top:5px; right:25px;">
                                <a href="javascript:void();" onclick="removeAtt();"><span class="label label-danger"><i class="fa fa-remove"></i> 移除</span></a>
                            </div>
                          </div>
                      
                      </div>

                    </div>
                    <div class="box-footer">
                      <div class="pull-right">
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                        <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                      </div>
                    </div>
                  </div>
                </div>

            </div>
        </section>

      </div>

</asp:Content>


