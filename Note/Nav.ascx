﻿<%@ control language="C#" autoeventwireup="true" inherits="Note_Nav, App_Web_msipsbpb" %>

<div class="col-md-3">
    <asp:HyperLink ID="hlAdd" runat="server" CssClass="btn btn-primary btn-block margin-bottom"><i class="fa fa-pencil"></i> 新建</asp:HyperLink>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">笔记</h3>
        <div class="box-tools">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div id="NavDiv" runat="server" class="box-body no-padding">

    </div>
    </div>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">分类</h3>
    </div>
    <div class="box-body">
        <div class="input-group">
            <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" placeholder="笔记分类"></asp:TextBox>
            <div class="input-group-btn">
                <asp:Button ID="btnAdd" runat="server" Text="新增" CssClass="btn btn-primary btn-flat" onclick="btnAdd_Click" />
            </div>
        </div>
    </div>
    </div>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">类型</h3>
        <div class="box-tools">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div id="TypeDiv" runat="server" class="box-body no-padding">

    </div>
    </div>
</div>
