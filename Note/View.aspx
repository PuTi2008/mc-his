﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Note_View, App_Web_qp10cplf" %>

<%@ Register Src="Nav.ascx" TagName="Nav" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            笔记查看
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">笔记查看</li>
          </ol>
        </section>

        <section class="content">
            <div class="row">
        
                <MojoCube:Nav id="Nav" runat="server" />
    
                <div class="col-md-9">
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">查看</h3>
                    </div>
                    
                    <div class="box-body">
                      <div class="mailbox-read-info">
                        <h3><asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
                        <h5><asp:Label ID="lblFrom" runat="server" CssClass="mailbox-read-time"></asp:Label><asp:Label ID="lblDate" runat="server" CssClass="mailbox-read-time pull-right"></asp:Label></h5>
                      </div>
                      <div class="mailbox-read-message">
                          <asp:Label ID="lblDescription" runat="server"></asp:Label>
                      </div>
                    </div>

                    <div class="box-footer">
                      <div class="pull-right">
                        <asp:HyperLink ID="hlPrint" runat="server" CssClass="btn btn-default" NavigateUrl="javascript:McPrintHide('.col-md-3,.box-title,.box-footer');"><i class="fa fa-print"></i> 打印</asp:HyperLink>
                        <asp:LinkButton ID="lnbEdit" runat="server" CssClass="btn btn-primary" onclick="lnbEdit_Click"><i class="fa fa-edit"></i> 编辑</asp:LinkButton>
                      </div>
                      <asp:LinkButton ID="lnbBack" runat="server" CssClass="btn btn-default" onclick="lnbBack_Click"><i class="fa fa-chevron-left"></i> 返回</asp:LinkButton>
                    </div>
                  </div>
                </div>

            </div>
        </section>

      </div>

</asp:Content>


