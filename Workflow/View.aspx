﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Workflow_View, App_Web_3nnyqhi4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            工作查看
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">工作查看</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <asp:HyperLink ID="hlHistory" runat="server"><span class="label label-warning"><i class="fa fa-history"></i> 历史</span></asp:HyperLink>
                  <asp:HyperLink ID="hlPrint" runat="server" NavigateUrl="javascript:McPrintHide('.box-header,.box-title,.box-footer,.main-footer');"><span class="label label-primary"><i class="fa fa-print"></i> 打印</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">

              <div id="ContentDiv" runat="server" class="row">

              </div>

            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnBack" runat="server" Text="返回" CssClass="btn btn-default" onclick="btnBack_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>