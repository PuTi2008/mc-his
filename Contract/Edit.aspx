﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Contract_Edit, App_Web_2t50sxjr" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function removeAtt() {
            document.getElementById("ctl00_cphMain_txtFilePath").value = "";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            合同管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">合同管理</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <asp:Label ID="lblOrderName" runat="server"></asp:Label>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">

                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="类型"></asp:Label></label>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label30" runat="server" Text="状态"></asp:Label></label>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label8" runat="server" Text="合同编号"></asp:Label></label>
                    <asp:TextBox ID="txtNumber" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label2" runat="server" Text="标题"></asp:Label></label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label6" runat="server" Text="合同金额"></asp:Label></label>
                    <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label5" runat="server" Text="签约时间"></asp:Label></label>
                    <asp:TextBox ID="txtSignDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label14" runat="server" Text="生效时间"></asp:Label></label>
                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label4" runat="server" Text="到期时间"></asp:Label></label>
                    <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label3" runat="server" Text="条款"></asp:Label></label>
                    <asp:TextBox ID="txtNote" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group" style="position:relative;">
                    <label><asp:Label ID="Label7" runat="server" Text="客户"></asp:Label></label>
                    <asp:HyperLink ID="hlCustomer" runat="server" Target="_blank" Visible="false"><i class="fa fa-external-link"></i></asp:HyperLink>
                    <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                    <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                    <div style="position:absolute; top:31px; right:20px;">
                        <asp:HyperLink ID="hlSearch" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-search"></i> 查找</span></asp:HyperLink>
                    </div>
                  </div>
                  
              </div>
              
              <div class="row">
                    
                  <div class="form-group" style="padding:15px">
                      <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                  </div>
                        
              </div>
                    
              <div class="row">
                  
                <hr />
                  
                <div class="col-md-6 form-group" style="position:relative;">
                    <asp:TextBox ID="txtFilePath" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                    <div style="position:absolute; top:5px; right:20px;">
                        <a href="javascript:void();" onclick="removeAtt();"><span class="label label-danger"><i class="fa fa-remove"></i> 移除</span></a>
                    </div>
                </div>

                <div class="col-md-6 form-group">
                <div class="btn btn-default btn-file">
                    <i class="fa fa-paperclip"></i> 增加附件
                    <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);"></asp:FileUpload>
                    <span id="filepath"></span>
                </div>
                <p class="help-block">5MB以内</p>
                </div>

              </div>

            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>