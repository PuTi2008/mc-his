﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Forum_View, App_Web_xkhhzvu0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function reset() {
            document.getElementById("ctl00_cphMain_txtContent").value = "";
        }

        function reply(content) {
            document.getElementById("ctl00_cphMain_txtContent").value = "回复：“" + decodeURI(content) + "”\n";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            主题查看
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">主题查看</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <a href="#ReplyDiv" class="fancybox" title="回复" onclick="reset();"><span class="label label-success"><i class="fa fa-mail-reply"></i> 回复</span></a>
              </h3>
            </div>

            <div class="box-body">
              <div class="row">
              
                <div class="box-body">
                  <div class="mailbox-read-info">
                    <h3><asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
                    <h5><asp:Label ID="lblFrom" runat="server" CssClass="mailbox-read-time"></asp:Label><asp:Label ID="lblDate" runat="server" CssClass="mailbox-read-time pull-right"></asp:Label></h5>
                  </div>
                  <div class="mailbox-read-message">
                  
                    <div id="VoteDiv" runat="server" visible="false" style="border:solid 1px #eee; overflow:auto">
                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table" OnRowDataBound="GridView2_RowDataBound" OnRowCommand="GridView2_RowCommand" OnRowCreated="GridView2_RowCreated" style="margin-bottom:0px;">
                            <Columns>
                                <asp:TemplateField HeaderText="投票">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVoteID" runat="server" Text='<%# Bind("pk_Vote") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblVisual" runat="server" Text='<%# Bind("Visual") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblTitleGV" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="400px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblVotes" runat="server" Text='<%# Bind("Votes") %>'></asp:Label>
                                        <asp:Label ID="lblBar" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="gvVote" runat="server" CommandName="_vote"><span class="label label-primary"><i class="fa fa-gavel"></i> 投票</span></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                    <asp:Label ID="lblDescription" runat="server"></asp:Label>

                  </div>
                  <div>
                  
                    <asp:GridView ID="ReplyGrid" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" style="margin-bottom:-30px" OnRowDataBound="ReplyGrid_RowDataBound" OnRowCommand="ReplyGrid_RowCommand" OnRowCreated="ReplyGrid_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="回复">
                                <ItemTemplate>
                                    
                                    <div class="post">
                                        <div class="user-block">
                                            <asp:Label ID="lblThumbnail" runat="server" Text='<%# Bind("ImagePath1") %>'></asp:Label>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Forum") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblParentID" runat="server" Text='<%# Bind("ParentID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblCreateUser" runat="server" Text='<%# Bind("CreateUser") %>' Visible="false"></asp:Label>
                                            <span class='username'>
                                                <a href="#"><asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label></a>
                                                <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CssClass="pull-right btn-box-tool" CommandName="_delete" Visible="false"><i class='fa fa-times'></i></asp:LinkButton>
                                            </span>
                                            <asp:Label ID="lblCreateDate" runat="server" Text='<%# Bind("CreateDate") %>' CssClass="description"></asp:Label>
                                        </div>
                                        <p>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                        </p>
                                        <ul class="list-inline">
                                            <li>
                                                <asp:HyperLink ID="gvReply" runat="server" CssClass="fancybox link-black text-sm" ToolTip="回复" NavigateUrl="#ReplyDiv"><i class="fa fa-mail-reply margin-r-5"></i> 回复</asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="gvLike" runat="server" CssClass="link-black text-sm" CommandName="_like"><i class="fa fa-thumbs-o-up margin-r-5"></i> 喜欢 (<asp:Label ID="lblLikes" runat="server" Text='<%# Bind("Likes") %>'></asp:Label>)</asp:LinkButton>
                                            </li>
                                            <li class="pull-right"><asp:Label ID="lblFloor" runat="server" Text='<%# Bind("SortID") %>'></asp:Label> 楼</li>
                                        </ul>
                                    </div>

                                </ItemTemplate>
                                <HeaderStyle BackColor="#eeeeee" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                  </div>
                </div>

              </div>
            </div>
            
            <div class="box-footer">
                <a href="#ReplyDiv" class="fancybox btn btn-primary" title="回复" onclick="reset();">回复</a>
                <asp:Button ID="btnBack" runat="server" Text="返回" CssClass="btn btn-default" onclick="btnBack_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>
      
      <div id="ReplyDiv" style="display:none; width:500px;">
            
          <div class="box-body">

              <div class="form-group">
                  <asp:TextBox ID="txtContent" runat="server" CssClass="form-control" TextMode="MultiLine" Height="150px"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbReply" runat="server" CssClass="btn btn-primary" onclick="lnbReply_Click">确定</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>


